# KTG-Challange

Using jQuery or vanilla JS you will display each 'USER' in a table. When the user selects a 'USER' in the table, it will display all of the 'POSTS' that were created by that 'USER'. You have full freedom in how you accomplish the above objectives.